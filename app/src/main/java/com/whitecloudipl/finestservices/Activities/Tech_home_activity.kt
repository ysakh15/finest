package com.whitecloudipl.finestservices.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.whitecloudipl.finestservices.Fragments.*
import com.whitecloudipl.finestservices.R
import com.whitecloudipl.finestservices.Utils.Common.getprefstring
import com.whitecloudipl.finestservices.Utils.Common.showtoast
import kotlinx.android.synthetic.main.activity_tech_home_activity.*

class Tech_home_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tech_home_activity)
        if(this.getprefstring(this,"cid")!=""){
            supportFragmentManager.beginTransaction().replace(R.id.tframe,Tech_Request()).commit()
        }
        else{
            this.showtoast(this,"Please Login First")
        }
        services.setOnClickListener {
            if(this.getprefstring(this,"cid")!=""){
                supportFragmentManager.beginTransaction().replace(R.id.tframe,Tech_Request()).commit()
            }
            else{
                this.showtoast(this,"Please Login First")
            }
        }
        booking.setOnClickListener {
            if(this.getprefstring(this,"cid")!=""){
                supportFragmentManager.beginTransaction().replace(R.id.tframe,Tech_Request_History()).commit()
            }
            else{
                this.showtoast(this,"Please Login First")
            }

        }
        notif.setOnClickListener {
            if(this.getprefstring(this,"cid")!=""){
                supportFragmentManager.beginTransaction().replace(R.id.tframe,Notifications()).commit()
            }
            else{
                this.showtoast(this,"Please Login First")
            }

        }
        profile.setOnClickListener {
            if(this.getprefstring(this,"cid")!=""){
                supportFragmentManager.beginTransaction().replace(R.id.tframe,More()).commit()
            }
            else{
                this.showtoast(this,"Please Login First")
            }

        }

    }




}
