package com.whitecloudipl.finestservices.Activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.github.piasy.biv.BigImageViewer
import com.github.piasy.biv.loader.glide.GlideImageLoader
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.whitecloudipl.finestservices.R
import com.whitecloudipl.finestservices.Utils.Common.putprefstring


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        BigImageViewer.initialize(GlideImageLoader.with(applicationContext))
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                val token = task.result!!.token
                putprefstring(applicationContext,"token",token)

            })
        Handler().postDelayed({
           startActivity(Intent(this,Home_Activity::class.java))
            finish()
        },0)
    }
}
