package com.whitecloudipl.finestservices.Activities

import android.content.Intent
import android.os.Bundle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.whitecloudipl.finestservices.Fragments.*
import com.whitecloudipl.finestservices.R
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.content_home_.*
import com.whitecloudipl.finestservices.Utils.Common.getprefstring
import com.whitecloudipl.finestservices.Utils.Common.showtoast

class Home_Activity : AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        var toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
        if(getprefstring(this,"type")=="4"&&getprefstring(this,"cid")!=""){
            startActivity(Intent(this,Tech_home_activity::class.java))
            finish()
        }
        else{
            supportFragmentManager.beginTransaction().replace(R.id.nav_frame, Services()).commit()
        }

        services.setBackgroundResource(R.color.colorPrimary)
        imageView1.setImageResource(R.drawable.servicewt)
        stxt.setTextColor(resources.getColor(R.color.white))

        aboutus.setOnClickListener {
        }
        register.setOnClickListener {
            val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
            drawer.closeDrawer(GravityCompat.START)
           supportFragmentManager.beginTransaction().replace(R.id.nav_frame,ChooseType()).commit()
        }

        terms.setOnClickListener {

        }

        login.setOnClickListener {
            val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
            drawer.closeDrawer(GravityCompat.START)
            supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Login()).commit()
        }

        notif.setOnClickListener {
            val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
            drawer.closeDrawer(GravityCompat.START)
            reset()
            more.setBackgroundResource(R.color.colorPrimary)
            imageView3.setImageResource(R.drawable.morewt)
            moretxt.setTextColor(resources.getColor(R.color.white))
            if(this.getprefstring(this,"cid")!=""){
                supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Notifications()).commit()
            }
            else{
                this.showtoast(this,"Please Login First")
            }

        }


        services.setOnClickListener {
            reset()
            services.setBackgroundResource(R.color.colorPrimary)
            imageView1.setImageResource(R.drawable.servicewt)
            stxt.setTextColor(resources.getColor(R.color.white))
            supportFragmentManager.beginTransaction().replace(R.id.nav_frame, Services()).commit()
        }

        booking.setOnClickListener {

            if(this.getprefstring(this,"cid")!=""){
                reset()
                booking.setBackgroundResource(R.color.colorPrimary)
                imageView2.setImageResource(R.drawable.bookingorderwt)
                btxt.setTextColor(resources.getColor(R.color.white))
                supportFragmentManager.beginTransaction().replace(R.id.nav_frame, Bookingorder()).commit()
            }
            else{
                this.showtoast(this,"Please Login First")
            }


        }


        more.setOnClickListener {
            reset()
            more.setBackgroundResource(R.color.colorPrimary)
            imageView3.setImageResource(R.drawable.morewt)
            moretxt.setTextColor(resources.getColor(R.color.white))
            supportFragmentManager.beginTransaction().replace(R.id.nav_frame, More()).commit()
        }

    }

    fun reset(){
        services.background=null
        booking.background=null
        more.background=null
        imageView1.setImageResource(R.drawable.servicebk)
        imageView2.setImageResource(R.drawable.bookingorder)
        imageView3.setImageResource(R.drawable.morebk)
        stxt.setTextColor(resources.getColor(R.color.black))
        btxt.setTextColor(resources.getColor(R.color.black))
        moretxt.setTextColor(resources.getColor(R.color.black))
    }

    override fun onSupportNavigateUp(): Boolean {
        return super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        var f:Fragment?=null
        if (id == R.id.nav_about) {
           // f = Notifications.newInstance()
        } else if (id == R.id.nav_terms) {
           // f = Meetinglist()
        } else if (id == R.id.nav_login) {
          //  f = Clientlist()
        } else if (id == R.id.nav_register) {
           // f = LeaveStatus()
        } else if (id == R.id.nav_notification) {
           // f = Addmeetingall()
        }
        if (f != null) {
            supportFragmentManager.beginTransaction().replace(R.id.nav_frame, f).commit()
        }
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
}
