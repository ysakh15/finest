package com.whitecloudipl.finestservices.Interfaces

import com.whitecloudipl.finestservices.Models.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @FormUrlEncoded
    @POST("addUser")
    fun register(@Field("username") username:String,@Field("email") email:String,@Field("password") password:String,
                 @Field("user_type") user_type:String,@Field("contact") contact:String):Call<RegisterResponse>
    @FormUrlEncoded
    @POST("login")
    fun login(@Field("email") email:String,@Field("password") password:String,@Field("device_type") device_type:String,
                 @Field("user_token") user_token:String):Call<LoginResponse>



    @Multipart
    @POST("addBooking")
    fun book(@Part("title") title: RequestBody?,@Part("customer_id") customer_id: RequestBody?,
             @Part("service_category_id") service_category_id: RequestBody?,@Part("service_id") service_id: RequestBody?,
             @Part("date") date: RequestBody?,@Part("time") time: RequestBody?,
             @Part("location") location: RequestBody?,@Part("description") description: RequestBody?,
             @Part("creator_id") creator_id: RequestBody?,@Part("task_id") user_id: RequestBody?,
             @Part attachment: List<MultipartBody.Part?>?): Call<GenResponse>

    @FormUrlEncoded
    @POST("getBooking")
    fun getbooking(@Field("customer_id") customer_id:String):Call<BookingResponse>

    @FormUrlEncoded
    @POST("getCustomerRequest")
    fun getCustomerRequest(@Field("technician_id") technician_id:String):Call<RequestResponse>

    @FormUrlEncoded
    @POST("getNotification")
    fun getnotif(@Field("assigner_id") assigner_id:String):Call<NotifResponse>

    @FormUrlEncoded
    @POST("saveBookingStatus")
    fun saveBookigStatus(@Field("booking_id") booking_id:String,@Field("booking_status") booking_status:String,
                         @Field("technician_accept_note") technician_accept_note:String,@Field("technician_id") technician_id:String):Call<RequestResponse>
}