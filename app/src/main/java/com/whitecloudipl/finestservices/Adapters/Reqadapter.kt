package com.whitecloudipl.finestservices.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.whitecloudipl.finestservices.Interfaces.HomeClick
import com.whitecloudipl.finestservices.Interfaces.Rclick
import com.whitecloudipl.finestservices.Models.RequestResponse
import com.whitecloudipl.finestservices.R

import kotlinx.android.synthetic.main.booking_item.view.*
import kotlinx.android.synthetic.main.fragment_tech__request.view.*
import kotlinx.android.synthetic.main.request_item.view.*
import kotlin.collections.ArrayList


/**
 * Created by user on 9/8/18.
 */

class Reqadapter(val context: Context, val list:ArrayList<RequestResponse.DataItem>,val rclick: Rclick) : RecyclerView.Adapter<Reqadapter.MyViewHolder>(),HomeClick {
    inner class MyViewHolder(internal var v: View) : RecyclerView.ViewHolder(v)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.request_item, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = list[position]
        val imglist =  data.gallery
        val adap = Imgadapter(context,imglist,this)
        holder.itemView.req_rv.layoutManager=LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        holder.itemView.req_rv.adapter=adap
        holder.itemView.cuname.setText(data.customername)
        holder.itemView.cphone.setText(data.customerphone)
        holder.itemView.sdet.setText(data.servicecategory+"/"+data.service)
        holder.itemView.tename.setText(data.technicianname)
        holder.itemView.tenumber.setText(data.technicianphone)
        holder.itemView.tenotes.setText(data.technician_accept_note)
        holder.itemView.location.setText(data.location)
        holder.itemView.atime.setText(data.technician_accept_time)
        holder.itemView.ctime.setText(data.technician_complete_time)
        if(data.booking_status=="completed"){
            holder.itemView.accept.visibility=View.GONE
            holder.itemView.comp.visibility=View.GONE
            holder.itemView.tenotes.setText(data.technician_complete_note)
        }
        if(data.booking_status=="accepted"){
            holder.itemView.accept.visibility=View.GONE
        }
        holder.itemView.accept.setOnClickListener {
            rclick.click(position,it,"")
        }
        holder.itemView.comp.setOnClickListener {
            rclick.click(position,it,"")
        }
    }
    override fun getItemCount(): Int {
        return list.size
    }

    override fun hclicked(pos: Int,img:String) {
        rclick.click(pos,null,img)
    }
}
