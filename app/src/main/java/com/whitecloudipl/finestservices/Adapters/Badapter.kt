package com.whitecloudipl.finestservices.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.whitecloudipl.finestservices.Interfaces.HomeClick
import com.whitecloudipl.finestservices.Models.BookingResponse
import com.whitecloudipl.finestservices.R
import kotlinx.android.synthetic.main.booking_item.view.*
import kotlinx.android.synthetic.main.service_item.view.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by user on 9/8/18.
 */

class Badapter(val context: Context,val list:ArrayList<BookingResponse.DataItem>) : RecyclerView.Adapter<Badapter.MyViewHolder>() {
    inner class MyViewHolder(internal var v: View) : RecyclerView.ViewHolder(v)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.booking_item, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = list[position]
        holder.itemView.cname.setText(data.customername)
        holder.itemView.phone.setText(data.customerphone)
        holder.itemView.serdet.setText(data.servicecategory+"/"+data.service)
        holder.itemView.tname.setText(data.technicianname)
        holder.itemView.tnumber.setText(data.technicianphone)
        holder.itemView.tnotes.setText(data.technician_accept_note)
        holder.itemView.aatime.setText(data.technician_accept_time)
        holder.itemView.cctime.setText(data.technician_complete_time)
    }
    override fun getItemCount(): Int {
        return list.size
    }
}
