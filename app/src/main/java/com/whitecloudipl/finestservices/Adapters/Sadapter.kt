package com.whitecloudipl.finestservices.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.whitecloudipl.finestservices.Interfaces.HomeClick
import com.whitecloudipl.finestservices.R
import kotlinx.android.synthetic.main.service_item.view.*
import java.util.*

/**
 * Created by user on 9/8/18.
 */

class Sadapter(
    internal var namearray: ArrayList<String>,
    internal var click: HomeClick
) : RecyclerView.Adapter<Sadapter.MyViewHolder>() {
    inner class MyViewHolder(internal var v: View) : RecyclerView.ViewHolder(v) {

        init {
        val tv = v.findViewById<TextView>(R.id.textView2)
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.service_item, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.v.setOnClickListener {
            click.hclicked(position,"")
        }
        holder.itemView.textView2.setText(namearray.get(position))
    }
    override fun getItemCount(): Int {
        return namearray.size
    }
}
