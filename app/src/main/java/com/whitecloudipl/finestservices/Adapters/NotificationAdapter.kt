package com.whitecloudipl.finestservices.Adapters

import android.app.Activity
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.whitecloudipl.finestservices.Models.NotifResponse
import com.whitecloudipl.finestservices.R


/**
 * Created by Android PC on 2/20/2017.
 */
class NotificationAdapter(
    var context: Context,
    productList: List<NotifResponse.DataItem>
) : RecyclerView.Adapter<NotificationAdapter.MyViewHolder?>() {
    private val modelList: List<NotifResponse.DataItem>
    var activity: Activity
    var search = ""

    inner class MyViewHolder(var v: View) : RecyclerView.ViewHolder(v) {
        var title: TextView
        var date: TextView
        var message: TextView

        init {
            title = v.findViewById(R.id.title)
            date = v.findViewById(R.id.date)
            message = v.findViewById(R.id.message)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.notification_item, parent, false)
        return MyViewHolder(itemView)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(
        holder: MyViewHolder,
        position: Int
    ) {
        holder.title.setText(modelList[position].bookingname)
        holder.date.setText(modelList[position].created)
        holder.message.setText(modelList[position].message)
    }

    override fun getItemCount(): Int {
        return modelList.size
    }

    init {
        modelList = productList
        activity = context as Activity
    }
}