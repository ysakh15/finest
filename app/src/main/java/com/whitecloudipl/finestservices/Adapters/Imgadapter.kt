package com.whitecloudipl.finestservices.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.whitecloudipl.finestservices.Interfaces.HomeClick
import com.whitecloudipl.finestservices.R
import kotlinx.android.synthetic.main.img_item.view.*

/**
 * Created by user on 9/8/18.
 */

class Imgadapter(
    val context: Context,
    internal var namearray: List<String>?,
    internal var click: HomeClick
) : RecyclerView.Adapter<Imgadapter.MyViewHolder>() {
    inner class MyViewHolder(internal var v: View) : RecyclerView.ViewHolder(v) {

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.img_item, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.v.setOnClickListener {
            click.hclicked(position,namearray!![position])
        }
        Glide
            .with(context)
            .load(namearray!![position])
            .centerCrop()
            .placeholder(R.drawable.roundlogo)
            .into(holder.itemView.img)
    }
    override fun getItemCount(): Int {
        return if (namearray!=null) namearray!!.size else 0
    }
}
