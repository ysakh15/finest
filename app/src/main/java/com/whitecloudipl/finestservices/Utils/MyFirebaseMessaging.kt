package com.whitecloudipl.finestservices.Utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.whitecloudipl.finestservices.R
import java.util.*
import com.whitecloudipl.finestservices.Utils.Common.putprefstring


class MyFirebaseMessaging:FirebaseMessagingService() {
    private lateinit var notificationManager: NotificationManager
    val CHANNEL_ID="finest"
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        //Setting up Notification channels for android O and above
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupNotificationChannels()
        }
        val notificationId = Random().nextInt(60000)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_stat_name)  //a resource for your custom small icon
            .setContentTitle("FinestServices") //the "title" value you sent in your notification
            .setContentText(message.data["title"]) //ditto
            .setAutoCancel(true)  //dismisses the notification on click
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText(message.data["message"]))
            .setSound(defaultSoundUri)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build())

    }






    override fun onNewToken(token: String) {
      applicationContext.putprefstring(applicationContext,"token",token)

    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupNotificationChannels() {
        val adminChannelName = "finestservices"
        val adminChannelDescription = "pushnotifications"
        val adminChannel: NotificationChannel
        adminChannel = NotificationChannel(CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH)
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        notificationManager.createNotificationChannel(adminChannel)
    }

}