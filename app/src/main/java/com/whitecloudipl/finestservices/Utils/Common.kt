package com.whitecloudipl.finestservices.Utils

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager.BadTokenException
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.whitecloudipl.finestservices.Interfaces.ApiInterface
import com.whitecloudipl.finestservices.R
import java.text.SimpleDateFormat
import java.util.*

object Common {
    var amcnonamc=""
    var servicetitle=""
    var servicecat=""
    var serviceid=""
     var dialog: Dialog?=null
    fun Context.showprogressdiaog(context: Context): Dialog {
            dialog = Dialog(context)
        val inflate = LayoutInflater.from(context).inflate(R.layout.progress_dialog, null)
        dialog!!.setContentView(inflate)
        dialog!!.setCancelable(false)
        dialog!!.window!!.setBackgroundDrawable(
            ColorDrawable(Color.TRANSPARENT))
        return dialog!!
    }
    fun Context.getclient(context: Context):ApiInterface {
        return ApiClient.getClient()!!.create(ApiInterface::class.java)
    }

    fun Context.showtoast(context: Context,msg:String) {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show()
    }

    fun Context.getprefstring(context: Context,key:String):String {
      return  context.getSharedPreferences("user",0).getString(key,"")
    }

    fun Context.putprefstring(context: Context,key:String,msg:String) {
        context.getSharedPreferences("user",0).edit().putString(key,msg).commit()
    }
    fun Context.getdate():String {
      return SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().time)
    }

    fun Context.gettime():String {
        return SimpleDateFormat("hh:mm a").format(Calendar.getInstance().time)
    }


}