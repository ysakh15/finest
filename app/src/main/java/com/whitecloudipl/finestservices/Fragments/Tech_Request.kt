package com.whitecloudipl.finestservices.Fragments


import android.R.attr.fragment
import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.whitecloudipl.finestservices.Adapters.Reqadapter
import com.whitecloudipl.finestservices.Interfaces.Rclick
import com.whitecloudipl.finestservices.Models.RequestResponse
import com.whitecloudipl.finestservices.R
import com.whitecloudipl.finestservices.Utils.Common.getclient
import com.whitecloudipl.finestservices.Utils.Common.getprefstring
import com.whitecloudipl.finestservices.Utils.Common.showprogressdiaog
import com.whitecloudipl.finestservices.Utils.Common.showtoast
import kotlinx.android.synthetic.main.activity_tech_home_activity.*
import kotlinx.android.synthetic.main.fragment_tech__request.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 */
class Tech_Request : Fragment(),Rclick {
    lateinit var dialog: Dialog
    lateinit var radapter: Reqadapter
    lateinit var rmanger: LinearLayoutManager
    val reqlist = arrayListOf<RequestResponse.DataItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dialog= context!!.showprogressdiaog(context!!)
        return inflater.inflate(R.layout.fragment_tech__request, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.toolbar.setTitleTextColor(Color.BLACK)
        activity!!.toolbar.title="REQUEST"
        radapter = Reqadapter(activity!!, reqlist,this)
        rmanger = LinearLayoutManager(activity)
        rvreq.adapter = radapter
        rvreq.layoutManager = rmanger
        getrequest()
        close.setOnClickListener {
            ll.visibility=View.GONE
        }
    }

    fun getrequest() {
        Log.e("curentactivity",activity.toString())
        reqlist.clear()
        dialog.show()
        activity!!.getclient(activity!!)
            .getCustomerRequest(activity!!.getprefstring(activity!!, "cid")).enqueue(object :
            Callback<RequestResponse> {
            override fun onFailure(call: Call<RequestResponse>?, t: Throwable?) {
                dialog.dismiss()
                activity!!.showtoast(activity!!, t!!.localizedMessage)
            }
            override fun onResponse(
                call: Call<RequestResponse>?,
                response: Response<RequestResponse>?
            ) {
                val resp = response?.body()
                if (resp?.status == 1) {
                    dialog.dismiss()
                    for(item in resp.data!!){
                        if(item.booking_status!="completed"){
                            reqlist.add(item)
                        }
                    }
                    radapter.notifyDataSetChanged()
                } else {
                    dialog.dismiss()
                    activity!!.showtoast(activity!!, resp?.msg!!)
                }
            }
        })
    }
    override fun click(pos: Int,view: View?,img:String) {
        if(view!=null){
            when(view?.id){
                R.id.accept->{
                    if(reqlist[pos].booking_status=="requested")  showdialog(pos,2) else activity!!.showtoast(activity!!,"Already Accepted")

                }
                R.id.comp->{
                    if(reqlist[pos].booking_status=="accepted")  showdialog(pos,3) else activity!!.showtoast(activity!!,"Accept the request first")

                }
            }
        }
        else{
          ll.visibility=View.VISIBLE
            Glide
                .with(activity!!)
                .load(img)
                .centerCrop()
                .placeholder(R.drawable.roundlogo)
                .into(mBigImage)
        }

    }

    fun showdialog(pos: Int, i: Int){
        val bu = AlertDialog.Builder(activity).create()
        val vi = LayoutInflater.from(activity).inflate(R.layout.tech_dialog,null)
        bu.setView(vi)
        val eta = vi.findViewById<EditText>(R.id.eta)
        val note = vi.findViewById<EditText>(R.id.note)
        val close = vi.findViewById<Button>(R.id.button5)
        if(i==3){
            eta.visibility=View.GONE
        }
        close.setOnClickListener {
            bu.dismiss()
            changestatus(pos,i.toString(),"ETA: "+eta.text.toString()+"/"+note.text.toString())
        }
        bu.show()
    }

    fun changestatus(pos: Int,status:String,note:String){
        dialog.show()
        val data =reqlist.get(pos)
        activity!!.getclient(activity!!).saveBookigStatus(data.id,status,note,data.technician_id).enqueue(object :Callback<RequestResponse>{
            override fun onFailure(call: Call<RequestResponse>?, t: Throwable?) {
                dialog.dismiss()
                activity!!.showtoast(activity!!,t!!.localizedMessage)
            }
            override fun onResponse(
                call: Call<RequestResponse>?,
                response: Response<RequestResponse>?
            ) {
                val resp = response?.body()
                if (resp?.status == 1) {
                    dialog.dismiss()
                    activity!!.showtoast(activity!!, resp?.msg!!)
                } else {
                    dialog.dismiss()
                    activity!!.showtoast(activity!!, resp?.msg!!)
                }
                getrequest()
            }

        })
    }



}
