package com.whitecloudipl.finestservices.Fragments


import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.whitecloudipl.finestservices.R
import com.whitecloudipl.finestservices.Utils.Common
import kotlinx.android.synthetic.main.app_bar_home_.*
import kotlinx.android.synthetic.main.fragment_choose_type.*

/**
 * A simple [Fragment] subclass.
 */
class ChooseType : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.toolbar.setTitleTextColor(Color.BLACK)
        activity!!.toolbar.title="CHOOSE CLIENT TYPE"
        amc.setOnClickListener {
            Common.amcnonamc="2"
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Login()).addToBackStack(null).commit()
        }
        nonamc.setOnClickListener {
            Common.amcnonamc="3"
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Login()).addToBackStack(null).commit()
        }
    }


}
