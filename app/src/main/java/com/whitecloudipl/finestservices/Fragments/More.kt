package com.whitecloudipl.finestservices.Fragments


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.whitecloudipl.finestservices.Activities.Home_Activity
import com.whitecloudipl.finestservices.Utils.Common.getprefstring
import com.whitecloudipl.finestservices.Utils.Common.showtoast
import com.whitecloudipl.finestservices.R
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home_.*
import kotlinx.android.synthetic.main.fragment_more.*

/**
 * A simple [Fragment] subclass.
 */
class More : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_more, container, false)
    }





    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.toolbar.setTitleTextColor(Color.BLACK)
        activity!!.toolbar.title="MORE"
        logout.setOnClickListener {
            activity!!.getSharedPreferences("user",0).edit().remove("cid").commit()
            activity!!.showtoast(activity!!,"User logged out")
            if(activity!!.getprefstring(activity!!,"type")=="4"){
                startActivity(Intent(activity,Home_Activity::class.java))
                activity!!.finish()
            }
        }

        notifi.setOnClickListener {
            if(activity!!.getprefstring(activity!!,"type")=="4"){
                activity!!.supportFragmentManager.beginTransaction().replace(R.id.tframe,Notifications()).commit()
            }
            else{
                activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Notifications()).commit()
            }
        }
    }


}
