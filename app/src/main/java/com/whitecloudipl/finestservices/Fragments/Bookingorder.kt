package com.whitecloudipl.finestservices.Fragments

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.whitecloudipl.finestservices.Adapters.Badapter
import com.whitecloudipl.finestservices.Models.BookingResponse
import com.whitecloudipl.finestservices.R
import kotlinx.android.synthetic.main.app_bar_home_.*
import kotlinx.android.synthetic.main.fragment_bookingorder.*
import com.whitecloudipl.finestservices.Utils.Common
import com.whitecloudipl.finestservices.Utils.Common.getclient
import com.whitecloudipl.finestservices.Utils.Common.showprogressdiaog
import com.whitecloudipl.finestservices.Utils.Common.getprefstring
import com.whitecloudipl.finestservices.Utils.Common.showtoast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class Bookingorder : Fragment() {
    lateinit var badapter:Badapter
    lateinit var bmanger:LinearLayoutManager
    lateinit var dialog: Dialog
    val list = arrayListOf<BookingResponse.DataItem>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bookingorder, container, false)
    }




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog= activity!!.showprogressdiaog(activity!!)
        activity!!.toolbar.setTitleTextColor(Color.BLACK)
        activity!!.toolbar.title="BOOKING"
        bmanger= LinearLayoutManager(activity)
        badapter = Badapter(activity!!,list)
        rvbooking.adapter=badapter
        rvbooking.layoutManager=bmanger
        getbookings()
        ongoing.setTextColor(Color.parseColor("#00ADDE"))
        ongoing.setOnClickListener {
            ongoing.setTextColor(Color.parseColor("#00ADDE"))
            history.setTextColor(Color.parseColor("#000000"))
            getbookings()
        }
        history.setOnClickListener {
            history.setTextColor(Color.parseColor("#00ADDE"))
            ongoing.setTextColor(Color.parseColor("#000000"))
          getbookingshistory()
        }
    }

    fun getbookings(){
        list.clear()
        dialog.show()
        activity!!.getclient(activity!!).getbooking(activity!!.getprefstring(activity!!,"cid")).enqueue(object :Callback<BookingResponse>{
            override fun onFailure(call: Call<BookingResponse>?, t: Throwable?) {
                dialog.dismiss()
                activity!!.showtoast(activity!!,t!!.localizedMessage)
            }
            override fun onResponse(
                call: Call<BookingResponse>?,
                response: Response<BookingResponse>?
            ) {
                try {
                    val resp = response?.body()
                    if(resp?.status==1){
                        dialog.dismiss()
                        for(item in resp.data!!){
                            if(item.booking_status!="completed"){
                                list.add(item)
                            }
                        }

                        badapter.notifyDataSetChanged()
                    }
                    else{
                        dialog.dismiss()
                        activity!!.showtoast(activity!!,resp?.msg!!)
                    }
                } catch (e: Exception) {

                }
            }
        })
    }

    fun getbookingshistory(){
        list.clear()
        dialog.show()
        activity!!.getclient(activity!!).getbooking(activity!!.getprefstring(activity!!,"cid")).enqueue(object :Callback<BookingResponse>{
            override fun onFailure(call: Call<BookingResponse>?, t: Throwable?) {
                dialog.dismiss()
                activity!!.showtoast(activity!!,t!!.localizedMessage)
            }
            override fun onResponse(
                call: Call<BookingResponse>?,
                response: Response<BookingResponse>?
            ) {
                try {
                    val resp = response?.body()
                    if(resp?.status==1){
                        dialog.dismiss()
                        for(item in resp.data!!){
                            if(item.booking_status=="completed"){
                                list.add(item)
                            }
                        }

                        badapter.notifyDataSetChanged()
                    }
                    else{
                        dialog.dismiss()
                        activity!!.showtoast(activity!!,resp?.msg!!)
                    }
                } catch (e: Exception) {

                }
            }
        })
    }
}
