package com.whitecloudipl.finestservices.Fragments


import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.whitecloudipl.finestservices.Models.RegisterResponse
import com.whitecloudipl.finestservices.R
import com.whitecloudipl.finestservices.Utils.Common
import com.whitecloudipl.finestservices.Utils.Common.getclient
import com.whitecloudipl.finestservices.Utils.Common.showprogressdiaog
import com.whitecloudipl.finestservices.Utils.Common.showtoast
import kotlinx.android.synthetic.main.app_bar_home_.*
import kotlinx.android.synthetic.main.fragment_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class Register : Fragment() {
    lateinit  var dialog: Dialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog= activity!!.showprogressdiaog(activity!!)


    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.toolbar.setTitleTextColor(Color.BLACK)
        activity!!.toolbar.title="REGISTER"
        textView3.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Login()).addToBackStack(null).commit()
        }
        button.setOnClickListener {
            register()
           // activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Login()).addToBackStack(null).commit()
        }
    }

    fun register(){
        dialog.show()
        activity!!.getclient(activity!!).register(name.text.toString(),email.text.toString(),pass.text.toString(),Common.amcnonamc,mobno.text.toString()).enqueue(object :Callback<RegisterResponse>{
            override fun onFailure(call: Call<RegisterResponse>?, t: Throwable?) {
                dialog.dismiss()
                activity!!.showtoast(activity!!,t!!.localizedMessage)
            }
            override fun onResponse(
                call: Call<RegisterResponse>?,
                response: Response<RegisterResponse>?
            ) {
              val resp = response?.body()
                if(resp?.status==1){
                    dialog.dismiss()
                    activity!!.showtoast(activity!!,"Registration Succesfull")
                    activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Login()).addToBackStack(null).commit()
                }
                else{
                    dialog.dismiss()
                    activity!!.showtoast(activity!!,resp?.msg!!)
                }
            }
        })
    }
}

