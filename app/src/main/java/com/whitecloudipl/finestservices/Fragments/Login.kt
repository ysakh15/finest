package com.whitecloudipl.finestservices.Fragments


import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.whitecloudipl.finestservices.Activities.Tech_home_activity
import com.whitecloudipl.finestservices.Models.LoginResponse
import com.whitecloudipl.finestservices.Utils.Common
import com.whitecloudipl.finestservices.Utils.Common.getclient
import com.whitecloudipl.finestservices.Utils.Common.showprogressdiaog
import com.whitecloudipl.finestservices.Utils.Common.showtoast
import com.whitecloudipl.finestservices.Utils.Common.putprefstring
import com.whitecloudipl.finestservices.Utils.Common.getprefstring
import com.whitecloudipl.finestservices.R
import kotlinx.android.synthetic.main.app_bar_home_.*
import kotlinx.android.synthetic.main.fragment_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class Login : Fragment() {
  lateinit  var dialog:Dialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e("onAttach",context.toString())
        dialog= activity!!.showprogressdiaog(activity!!)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.toolbar.setTitleTextColor(Color.BLACK)
        activity!!.toolbar.title="LOGIN"
        textView3.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Register()).addToBackStack(null).commit()
        }
        button.setOnClickListener {
            login()
            //activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,ServiceRequest()).addToBackStack(null).commit()
        }
    }

    fun login(){
        dialog.show()
        activity!!.getclient(activity!!).login(email.text.toString(),pass.text.toString(),"android",activity!!.getprefstring(activity!!,"token")).
            enqueue(object :Callback<LoginResponse>{
                override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                    dialog.dismiss()
                    activity!!.showtoast(activity!!,t!!.localizedMessage)
                }
                override fun onResponse(
                    call: Call<LoginResponse>?,
                    response: Response<LoginResponse>?
                ) {
                    val resp = response?.body()
                    if(resp?.status==1){
                        dialog.dismiss()
                        activity!!.putprefstring(activity!!,"cid", resp.data?.get(0)!!.id)
                        activity!!.putprefstring(activity!!,"type", resp.data?.get(0).user_type)
                        activity!!.putprefstring(activity!!,"name", resp.data?.get(0).username)
                        activity!!.putprefstring(activity!!,"num", resp.data?.get(0).contact)
                        activity!!.showtoast(activity!!,"Login Succesfull")
                        if(resp.data?.get(0).user_type=="4"){
                         startActivity(Intent(activity,Tech_home_activity::class.java))
                            activity!!.finish()
                        }
                        else{
                            activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Services()).addToBackStack(null).commit()
                        }
                    }
                    else{
                        dialog.dismiss()
                        activity!!.showtoast(activity!!,resp?.msg!!)
                    }
                }

            })
    }
}
