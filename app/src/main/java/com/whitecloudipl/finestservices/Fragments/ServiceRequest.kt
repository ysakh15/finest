package com.whitecloudipl.finestservices.Fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.schibstedspain.leku.*
import com.whitecloudipl.finestservices.Models.GenResponse
import com.whitecloudipl.finestservices.R
import com.whitecloudipl.finestservices.Utils.Common
import com.whitecloudipl.finestservices.Utils.Common.showprogressdiaog
import com.whitecloudipl.finestservices.Utils.Common.getclient
import com.whitecloudipl.finestservices.Utils.Common.gettime
import com.whitecloudipl.finestservices.Utils.Common.getdate
import com.whitecloudipl.finestservices.Utils.Common.getprefstring
import com.whitecloudipl.finestservices.Utils.Common.showtoast
import kotlinx.android.synthetic.main.app_bar_home_.*
import kotlinx.android.synthetic.main.fragment_service_request.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Multipart
import java.io.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ServiceRequest : Fragment() {
    val MAP_BUTTON_REQUEST_CODE=100
    lateinit var dialog: Dialog
    val imglist = arrayListOf<String>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_service_request, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       dialog= activity!!.showprogressdiaog(activity!!)
        activity!!.toolbar.setTitleTextColor(Color.BLACK)
        activity!!.toolbar.title="SERVICE REQUEST"
        button2.setOnClickListener {
            book()
           // activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Services()).commit()
        }

        button3.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,Services()).commit()
        }
        val locationPickerIntent = LocationPickerActivity.Builder()
            //  .withLocation(41.4036299, 2.1743558)
            .withGeolocApiKey("AIzaSyCoWdWvHzx6d_NieUkbOfKFFMeX4fouLos")
           // .withSearchZone("en_IN")
            // .withSearchZone(SearchZoneRect(LatLng(26.525467, -18.910366), LatLng(43.906271, 5.394197)))
            // .withDefaultLocaleSearchZone()
            .shouldReturnOkOnBackPressed()
            .withStreetHidden()
            .withCityHidden()
            .withZipCodeHidden()
            .withSatelliteViewHidden()
            .withGooglePlacesEnabled()
            .withGoogleTimeZoneEnabled()
            .withVoiceSearchHidden()
            .withUnnamedRoadHidden()
            .build(activity!!)
        location.setOnClickListener {
            startActivityForResult(locationPickerIntent, MAP_BUTTON_REQUEST_CODE)
        }
        image.setOnClickListener {
        checkPermissionstorageforpicker()
        }

        name.setText(activity!!.getprefstring(activity!!,"name"))
        phone.setText(activity!!.getprefstring(activity!!,"num"))
        service.setText(Common.servicetitle)
    }
    fun checkPermissionstorageforpicker(): Boolean {
        return if (ContextCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            !== PackageManager.PERMISSION_GRANTED
        ) { // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) { // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                AlertDialog.Builder(activity!!)
                    .setTitle("Allow Permission")
                    .setMessage("To Access your Storage")
                    .setPositiveButton("OK",
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                400
                            )
                        })
                    .create()
                    .show()
            } else { // No explanation needed, we can request the permission.
                requestPermissions(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    400
                )
            }
            false
        } else {
            try {
                openpicker()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            true
        }
    }
    fun openpicker() {
        val intent = Intent()
        intent.type = "*/*"
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Choose File"), 11)
    }
    fun writefile(uri: Uri?) {
        var `in`: InputStream? = null
        var out: OutputStream? = null
        var locfile: File? = null
        try {
            val folder =
                File(Environment.getExternalStorageDirectory().toString() + File.separator + "EmployeeApp" + File.separator + "Send" + File.separator)
            if (!folder.exists()) {
                folder.mkdirs()
            }
            val mimetype =
                activity!!.applicationContext.contentResolver.getType(uri)
            Log.e("mimetype", mimetype)
            var fextention = ""
            if (mimetype.contains("pdf")) {
                fextention = "pdf"
            } else if (mimetype.contains("vnd.openxmlformats-officedocument.wordprocessingml.document")) {
                fextention = "docx"
            } else if (mimetype.contains("jpeg")) {
                fextention = "jpeg"
            } else if (mimetype.contains("png")) {
                fextention = "png"
            } else if (mimetype.contains("mp4")) {
                fextention = "mp4"
            }
            locfile = File(
                folder,
                System.currentTimeMillis().toString() + "." + fextention
            )
            // open the user-picked file for reading:
            if (!locfile.exists()) {
                locfile.createNewFile()
            }
            `in` = activity!!.applicationContext.contentResolver.openInputStream(uri)
            // open the output-file:
            out = FileOutputStream(locfile)
            // copy the content:
            val buffer = ByteArray(1024)
            var len: Int
            while (`in`.read(buffer).also { len = it } != -1) {
                out.write(buffer, 0, len)
            }
            // Contents are copied!
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            if (`in` != null) {
                try {
                    `in`.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            if (out != null) {
                try {
                    out.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            if (locfile != null) {
                Log.e("filepath", locfile.absolutePath)
                imglist.add(locfile.absolutePath)
                //fileupload(locfile.getAbsolutePath());
            } else {
                Toast.makeText(activity, "Please Try again", Toast.LENGTH_SHORT).show()
            }
        }
    }
    fun book(){
        dialog.show()
        val filelist: MutableList<MultipartBody.Part> = ArrayList()
        for (i in imglist.indices) {
            val upfile = File(imglist[i])
            Log.e( "fillist",filelist.size.toString())
            val requestBody =
                RequestBody.create("file/*".toMediaTypeOrNull(),upfile)
            val singlefile = MultipartBody.Part.createFormData(
                "attachment[]",
                upfile.name,
                requestBody
            )
            filelist.add(singlefile)
        }
        activity!!.getclient(activity!!).book(getreqbody(Common.servicetitle),getreqbody(activity!!.getprefstring(activity!!,"cid")),
            getreqbody(Common.servicecat),getreqbody(Common.serviceid),getreqbody(activity!!.getdate()),getreqbody(activity!!.getdate())
        ,getreqbody(location.text.toString()),getreqbody(desc.text.toString()),getreqbody(activity!!.getprefstring(activity!!,"cid")),getreqbody(activity!!.getprefstring(activity!!,"cid"))
            ,filelist).enqueue(object :Callback<GenResponse>{
            override fun onFailure(call: Call<GenResponse>?, t: Throwable?) {
                dialog.dismiss()
                activity!!.showtoast(activity!!,t!!.localizedMessage)
            }
            override fun onResponse(call: Call<GenResponse>?, response: Response<GenResponse>?) {
                val resp = response?.body()
                if(resp?.status==1){
                    dialog.dismiss()
                    activity!!.showtoast(activity!!,resp?.msg!!)
                }
                else{
                    dialog.dismiss()
                    activity!!.showtoast(activity!!,resp?.msg!!)
                }
            }
        })
    }
    fun getreqbody(str:String):RequestBody{
        return RequestBody.create(MultipartBody.FORM, str)
    }
    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT****", "OK")
            if (requestCode == MAP_BUTTON_REQUEST_CODE) {
                val latitude = data.getDoubleExtra(LATITUDE, 0.0)
                Log.d("LATITUDE****", latitude.toString())
                val longitude = data.getDoubleExtra(LONGITUDE, 0.0)
                Log.d("LONGITUDE****", longitude.toString())
                val address = data.getStringExtra(LOCATION_ADDRESS)
                Log.d("ADDRESS****", address.toString())
                val postalcode = data.getStringExtra(ZIPCODE)
                Log.d("POSTALCODE****", postalcode.toString())
                /* val bundle = data.getBundleExtra(TRANSITION_BUNDLE)
                 Log.d("BUNDLE TEXT****", bundle.getString("test"))*/
               // val fullAddress = data.getParcelableExtra<Address>(ADDRESS)
                val fullAddress = data.getStringExtra(LOCATION_ADDRESS)
                if (fullAddress != null) {
                    Log.d("FULL ADDRESS****", fullAddress.toString())
                    location.setText(fullAddress.toString())
                }
                /*   val timeZoneId = data.getStringExtra(TIME_ZONE_ID)
                   Log.d("TIME ZONE ID****", timeZoneId)
                   val timeZoneDisplayName = data.getStringExtra(TIME_ZONE_DISPLAY_NAME)
                   Log.d("TIME ZONE NAME****", timeZoneDisplayName)*/
            } else if (requestCode == 2) {
                val latitude = data.getDoubleExtra(LATITUDE, 0.0)
                Log.d("LATITUDE****", latitude.toString())
                val longitude = data.getDoubleExtra(LONGITUDE, 0.0)
                Log.d("LONGITUDE****", longitude.toString())
                val address = data.getStringExtra(LOCATION_ADDRESS)
                Log.d("ADDRESS****", address.toString())
                val lekuPoi = data.getParcelableExtra<LekuPoi>(LEKU_POI)
                Log.d("LekuPoi****", lekuPoi.toString())
            }
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("RESULT****", "CANCELLED")
        }

        if (requestCode == 11 && resultCode == Activity.RESULT_OK) { /*    Log.e("uri", data.getData().toString());
            Log.e("uripath", data.getData().getPath());
            Log.e("urilastpath", data.getData().getLastPathSegment());
            Log.e("filename", data.getData().getLastPathSegment().substring(data.getData().getLastPathSegment().lastIndexOf("/") + 1));
*/
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    if (data!!.clipData != null) {
                        for (i in 0 until data.clipData.itemCount) {
                            if (data.clipData.getItemAt(i).uri.lastPathSegment.contains(".")) {
                                writefile(
                                    data.clipData.getItemAt(i).uri,
                                    data.clipData.getItemAt(i).uri.lastPathSegment.substring(
                                        data.clipData.getItemAt(i).uri.lastPathSegment.lastIndexOf(
                                            "/"
                                        ) + 1
                                    )
                                )
                            } else {
                                writefile(data.clipData.getItemAt(i).uri)
                            }
                        }
                        try {
                            image.setText(data.clipData.itemCount.toString()+" file/s added")
                        } catch (e: Exception) {
                        }

                    } else {
                        if (data.data.lastPathSegment.contains(".")) {
                            writefile(
                                data.data,
                                data.data.lastPathSegment.substring(
                                    data.data.lastPathSegment.lastIndexOf("/") + 1
                                )
                            )
                            try {
                                image.setText(data.clipData.itemCount.toString()+" file/s added")
                            } catch (e: Exception) {
                            }

                        } else {
                            writefile(data.data)
                            try {
                                image.setText(data.clipData.itemCount.toString()+" file/s added")
                            } catch (e: Exception) {
                            }
                        }
                    }
                } else {
                    if (data!!.data.lastPathSegment.contains(".")) {
                        writefile(
                            data.data,
                            data.data.lastPathSegment.substring(
                                data.data.lastPathSegment.lastIndexOf("/") + 1
                            )
                        )
                        try {
                            image.setText(data.clipData.itemCount.toString()+" file/s added")
                        } catch (e: Exception) {
                        }
                    } else {
                        writefile(data.data)
                        try {
                            image.setText(data.clipData.itemCount.toString()+" file/s added")
                        } catch (e: Exception) {
                        }
                    }
                }
            } catch (e: Exception) {
                activity!!.showtoast(activity!!,"File cannot be send")
            }
        }
    }
    fun writefile(uri: Uri?, fname: String?) {
        Log.e("writefiename", fname)
        var `in`: InputStream? = null
        var out: OutputStream? = null
        var locfile: File? = null
        try {
            val folder =
                File(Environment.getExternalStorageDirectory().toString() + File.separator + "FinestServices" + File.separator + "Send" + File.separator)
            if (!folder.exists()) {
                folder.mkdirs()
            }
            locfile = File(folder, fname)
            // open the user-picked file for reading:
            if (!locfile.exists()) {
                locfile.createNewFile()
            }
            `in` = activity!!.applicationContext.contentResolver.openInputStream(uri)
            // open the output-file:
            out = FileOutputStream(locfile)
            // copy the content:
            val buffer = ByteArray(1024)
            var len: Int
            while (`in`.read(buffer).also { len = it } != -1) {
                out.write(buffer, 0, len)
            }
            // Contents are copied!
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            if (`in` != null) {
                try {
                    `in`.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            if (out != null) {
                try {
                    out.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
        if (locfile != null) {
            Log.e("filepath1", locfile.absolutePath)
            imglist.add(locfile.absolutePath)
            // fileupload(locfile.getAbsolutePath());
        }
        else {
            Toast.makeText(activity, "Please Try again", Toast.LENGTH_SHORT).show()
        }
            Log.e("filenames","start")
            image.setText(imglist.size.toString()+"file/s added")

    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.size > 0
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            try {
                openpicker()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        } else {
            Toast.makeText(activity, "Please Provide Storage Permisiion", Toast.LENGTH_SHORT)
                .show()
        }
    }
}
