package com.whitecloudipl.finestservices.Fragments


import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.whitecloudipl.finestservices.Adapters.NotificationAdapter
import com.whitecloudipl.finestservices.Models.NotifResponse
import com.whitecloudipl.finestservices.Utils.Common.getclient
import com.whitecloudipl.finestservices.Utils.Common.getprefstring
import com.whitecloudipl.finestservices.Utils.Common.showprogressdiaog

import com.whitecloudipl.finestservices.R
import com.whitecloudipl.finestservices.Utils.Common.showtoast
import kotlinx.android.synthetic.main.activity_tech_home_activity.*
import kotlinx.android.synthetic.main.fragment_notifications.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class Notifications : Fragment() {
    lateinit var dialog: Dialog
    lateinit var nmanger:LinearLayoutManager
    lateinit var nadapter:NotificationAdapter
    val list = arrayListOf<NotifResponse.DataItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialog= activity!!.showprogressdiaog(context as Activity)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.toolbar.setTitleTextColor(Color.BLACK)
        activity!!.toolbar.title="NOTIFICATIONS"
        nmanger= LinearLayoutManager(activity!!)
        nadapter= NotificationAdapter(activity!!,list)
        rvnot.adapter=nadapter
        rvnot.layoutManager=nmanger
        getnotif()
    }
    fun getnotif(){
        list.clear()
        dialog.show()
        activity!!.getclient(activity!!).getnotif(activity!!.getprefstring(activity!!,"cid")).enqueue(object :
            Callback<NotifResponse>{
            override fun onFailure(call: Call<NotifResponse>?, t: Throwable?) {
                dialog.dismiss()
                activity!!.showtoast(activity!!,t!!.localizedMessage)
            }

            override fun onResponse(
                call: Call<NotifResponse>?,
                response: Response<NotifResponse>?
            ) {
                val resp = response?.body()
                if(resp?.status==1){
                    dialog.dismiss()
                    list.addAll(resp.data!!)
                    nadapter.notifyDataSetChanged()
                }
                else{
                    dialog.dismiss()
                    activity!!.showtoast(activity!!,resp?.msg!!)
                }
            }

        })



    }
}
