package com.whitecloudipl.finestservices.Fragments


import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.whitecloudipl.finestservices.Adapters.Sadapter
import com.whitecloudipl.finestservices.Interfaces.HomeClick
import com.whitecloudipl.finestservices.Utils.Common.getprefstring
import com.whitecloudipl.finestservices.R
import com.whitecloudipl.finestservices.Utils.Common
import kotlinx.android.synthetic.main.app_bar_home_.*
import kotlinx.android.synthetic.main.content_home_.*
import kotlinx.android.synthetic.main.fragment_services.*


/**
 * A simple [Fragment] subclass.
 */
class Services : Fragment(),HomeClick {
     var dialog:AlertDialog?=null
    val arrayList= arrayListOf<String>()
    val arrayList1= arrayListOf<String>()
    lateinit var vi:View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_services, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.toolbar.setTitleTextColor(Color.BLACK)
        activity!!.toolbar.title="SERVICES"
        dialog=AlertDialog.Builder(activity!!).create()
        vi =  LayoutInflater.from(activity).inflate(R.layout.dialog_item,null)
        ac.setOnClickListener {
            Common.servicecat="1"
            arrayList.clear()
            arrayList1.clear()
            arrayList.add("No Cooling")
            arrayList.add("No Power")
            arrayList.add("Water Leak")
            arrayList.add("AC Services")
            arrayList.add("Others")
            arrayList1.add("1")
            arrayList1.add("2")
            arrayList1.add("3")
            arrayList1.add("4")
            arrayList1.add("5")
            showdialog(arrayList)
        }
        electrical.setOnClickListener {
            Common.servicecat="2"
            arrayList.clear()
            arrayList1.clear()
            arrayList.add(" No Power")
            arrayList.add("Electrical Maintenance")
            arrayList.add("Others")
            arrayList1.add("6")
            arrayList1.add("7")
            arrayList1.add("8")
            showdialog(arrayList)
        }
        thermostat.setOnClickListener {
            Common.servicecat="4"
            arrayList.clear()
            arrayList1.clear()
            arrayList.add("Smart Thermostat installation")
            arrayList.add(" Normal Thermostat Installation")
            arrayList.add("Thermostat Maintenance")
            arrayList.add("Others")
            arrayList1.add("14")
            arrayList1.add("15")
            arrayList1.add("16")
            arrayList1.add("17")
            arrayList1.add("18")
            showdialog(arrayList)
        }
        plumbing.setOnClickListener {
            Common.servicecat="3"
            arrayList.clear()
            arrayList1.clear()
            arrayList.add(" Water Leak")
            arrayList.add("Drain Blockage")
            arrayList.add("Water heater")
            arrayList.add("Pipe/Pump System Maintenance")
            arrayList.add("Others")
            arrayList1.add("9")
            arrayList1.add("10")
            arrayList1.add("11")
            arrayList1.add("12")
            arrayList1.add("13")
            showdialog(arrayList)
        }
        other.setOnClickListener {
            Common.servicecat="5"
            arrayList.clear()
            arrayList1.clear()
            arrayList.add("General Work")
            arrayList1.add("")
           val bu = AlertDialog.Builder(activity).create()
            val vi = LayoutInflater.from(activity).inflate(R.layout.other_dialog,null)
            bu.setView(vi)
            val submit = vi.findViewById<Button>(R.id.button4)
            val close = vi.findViewById<Button>(R.id.button5)
            submit.setOnClickListener {
                bu.dismiss()
                if(activity!!.getprefstring(activity!!,"cid")!=""){
                    Common.serviceid=arrayList1.get(0)
                    Common.servicetitle=arrayList[0]
                    activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,ServiceRequest()).addToBackStack(null).commit()
                }
                else{
                    activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,ChooseType()).addToBackStack(null).commit()
                }
            }
            close.setOnClickListener {
                bu.dismiss()
            }
            bu.show()
        }
    }

    fun showdialog(list:ArrayList<String>){
        dialog?.setView(vi)
        val rv = vi.findViewById<RecyclerView>(R.id.rvdialog)
        val sadapter = Sadapter(list,this)
        val smanger = LinearLayoutManager(activity)
        rv.adapter=sadapter
        rv.layoutManager=smanger
        sadapter.notifyDataSetChanged()
        dialog?.show()
    }

    override fun hclicked(pos: Int, img: String) {
        Log.e("pos",pos.toString())
        dialog?.dismiss()
        if(activity!!.getprefstring(activity!!,"cid")!=""){
            Common.serviceid=arrayList1.get(pos)
            Common.servicetitle=arrayList[pos]
            //Common.servicetitle
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,ServiceRequest()).addToBackStack(null).commit()
        }
        else{
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_frame,ChooseType()).addToBackStack(null).commit()
        }

    }
}
