package com.whitecloudipl.finestservices.Models




data class RequestResponse(val msg: String = "",
                           val data: List<DataItem>?,
                           val status: Int = 0){
    data class DataItem(val servicecategory: String = "",
                        val created: String = "",
                        val service: String = "",
                        val creatorId: String = "",
                        val technician_id: String = "",
                        val technicianname: String = "",
                        val booking_status:String="",
                        val technicianphone:String="",
                        val technician_accept_note: String = "",
                        val technician_complete_note:String="",
                        val serviceCategoryId: String = "",
                        val customerphone: String = "",
                        val id: String = "",
                        val title: String = "",
                        val location: String = "",
                        val customername: String = "",
                        val gallery: List<String>?,
                        val technician_accept_time: String = "",
                        val technician_complete_time: String = "",
                        val status: String = "")
}


