package com.whitecloudipl.finestservices.Models




data class BookingResponse(val status: Int = 0,
                           val msg: String = "",
                           val data: List<DataItem>?){
    data class DataItem(val date: String = "",
                        val bookingCode: String = "",
                        val created: String = "",
                        val description: String = "",
                        val removeStatus: String = "",
                        val title: String = "",
                        val booking_status:String="",
                        val customername:String="",
                        val technicianphone:String="",
                        val technicianname:String="",
                        val service:String="",
                        val servicecategory:String="",
                        val serviceCategoryId: String = "",
                        val customerphone: String = "",
                        val serviceId: String = "",
                        val creatorId: String = "",
                        val technician_accept_note: String = "",
                        val technician_accept_time: String = "",
                        val technician_complete_time: String = "",
                        val modifierId: String = "",
                        val modified: String = "",
                        val location: String = "",
                        val id: String = "",
                        val technitionId: String = "",
                        val time: String = "",
                        val customerId: String = "",
                        val gallery: List<String>?,
                        val status: String = "")
}


