package com.whitecloudipl.finestservices.Models




data class TechResponse(val msg: String = "",
                        val data: List<DataItem>?,
                        val status: Int = 0){
    data class DataItem(val servicecategory: String = "",
                        val customerphone: String = "",
                        val created: String = "",
                        val service: String = "",
                        val creatorId: String = "",
                        val technicianId: String = "",
                        val technicianname: String = "",
                        val id: String = "",
                        val title: String = "",
                        val customername: String = "",
                        val status: String = "")

}


