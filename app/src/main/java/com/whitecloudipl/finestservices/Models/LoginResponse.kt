package com.whitecloudipl.finestservices.Models

 class LoginResponse(val status: Int = 0,
                         val msg: String = "",
                         val data: List<DataItem>?){
    data class DataItem(val tstatus: String = "",
                        val deviceType: String = "",
                        val removeStatus: String = "",
                        val password: String = "",
                        val userPriority: String = "",
                        val user_type: String = "",
                        val branchId: String = "",
                        val shiftId: String = "",
                        val contact: String = "",
                        val modified: String = "",
                        val id: String = "",
                        val userToken: String = "",
                        val fax: String = "",
                        val email: String = "",
                        val image: String = "",
                        val secretId: String = "",
                        val address: String = "",
                        val creatorType: String = "",
                        val created: String = "",
                        val officePhone: String = "",
                        val parentId: String = "",
                        val creatorId: String = "",
                        val modifierId: String = "",
                        val cpassword: String = "",
                        val username: String = "",
                        val status: String = "")
}





