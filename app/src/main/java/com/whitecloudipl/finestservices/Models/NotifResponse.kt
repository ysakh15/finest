package com.whitecloudipl.finestservices.Models




data class NotifResponse(val msg: String = "",
                         val data: List<DataItem>?,
                         val status: Int = 0){
    data class DataItem(val bookingId: String = "",
                        val bookingname: String = "",
                        val created: String = "",
                        val id: String = "",
                        val message: String = "")
}


